<div align=center>

<img src="https://about.gitlab.com/images/ci/gitlab-ci-cd-logo_2x.png" alt="Devops" width="200"/>

<!-- omit in toc -->
# DEVOPS I - DEMO

<h5> All commands </h5>

**Clément ROYER | HOFGAERTNER Tristan**

<!-- omit in toc -->
##

A **vue-js front-web app** connected to a **java spring boot restfull API** 
connected to a **MySql database**. Everything under **Docker** and with a good **ci & cd**

**[website](http://13.69.191.103/)**

</div>


## Proof of deployment

- edit `Controller`.`ping`
- push
- merge

## Starting dev in local

```
(cd MyProj-Backend; mvn install -DskipTests)
docker-compose build
docker-compose up -d
```

## Starting prod in local

```
docker-compose -f docker-compose.prod.yml down
docker-compose -f docker-compose.prod.yml pull
docker-compose -f docker-compose.prod.yml build
docker-compose -f docker-compose.prod.yml up -d
```

## Connect to server by ssh

``` CMD
winpty ssh root@13.69.191.103 -p 57666
```