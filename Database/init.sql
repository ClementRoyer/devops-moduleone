CREATE SCHEMA IF NOT EXISTS `MyProj_database`  DEFAULT CHARACTER SET `utf8` COLLATE `utf8_unicode_ci`;

CREATE USER 'admin'@'localhost' IDENTIFIED BY 'pass';
CREATE USER 'admin'@'%' IDENTIFIED BY 'pass';

GRANT ALL ON *.* TO 'admin'@'localhost';
GRANT ALL ON *.* TO 'admin'@'%';

CREATE TABLE IF NOT EXISTS `MyProj_database`.`User` (
    `ID` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `Name` LONGTEXT NOT NULL,
    `Email` LONGTEXT NOT NULL,
    `Creation` DATE NULL DEFAULT NULL,
    PRIMARY KEY (`ID`),
    UNIQUE INDEX `ID_UNIQUE` (`ID` ASC)
)
AUTO_INCREMENT = 0
DEFAULT CHARACTER SET = utf8;

DELIMITER $$
USE `MyProj_database`$$
DROP TRIGGER IF EXISTS `MyProj_database`.`MyProj_database_BEFORE_INSERT` $$
CREATE DEFINER=`admin`@`%` TRIGGER `MyProj_database`.`User_BEFORE_INSERT` BEFORE INSERT ON `User` FOR EACH ROW
BEGIN
    SET NEW.Creation = NOW();
END;$$