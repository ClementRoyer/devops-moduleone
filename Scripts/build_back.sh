# for prod only

cd MyProj-Backend;

mvn package -DskipTests;

mkdir -p target/dependency;

(cd target/dependency; jar -xf ../*.jar)

docker build -f Dockerfile.back.prod -t ciemrnt/my-proj_back .