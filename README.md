<div align=center>

<img src="https://about.gitlab.com/images/ci/gitlab-ci-cd-logo_2x.png" alt="Devops" width="200"/>

<!-- omit in toc -->
# DEVOPS I - 2021

**Clément ROYER | HOFGAERTNER Tristan**

<!-- omit in toc -->
##

A **vue-js front-web app** connected to a **java spring boot restfull API** 
connected to a **MySql database**. Everything under **Docker** and with a good **ci & cd**

**[website](http://13.69.191.103/)**

</div>

<!-- omit in toc -->
# 


- [CI - CD](#ci---cd)
- [GIT Workflow](#git-workflow)
- [Docker](#docker)
- [Database](#database)
- [Back-end](#back-end)
- [Front-end](#front-end)



## CI - CD

<img src="./.readme/pipelines.png" alt="pipelines"/>

> **All image used in production are build in this ci, you can check the result of junit test, and deploy to test or production your solution.**


|Stage|Master|Pre-production|dev|features|
|-:|:-:|:-:|:-:|:-:|
|Build|✅|✅|✅|✅|
|Test|✅|✅|✅|✅|
|Package|✅|✅|❌|❌|
|Deploy|✅|✅|❌|❌|

<details>
    <summary>.gitlab-ci</summary>

``` GITLAB-CI
stages:
    - build #                  1. Build and test the server & the database image
    - test #                   2. Test the API (test coverage here)
    - package  #               2. Create and push the new docker image. (Have to do some versioning)
    - deploy #                 3. Connect to the server by ssh, pull the latest image and run it.


variables:
    DOCKER_LOGIN: $CI_DOCKER_LOGIN
    DOCKER_PASS: $CI_DOCKER_PASS
    MAVEN_PWD: $CI_PROJECT_DIR/.m2
    SSH_PRIVATE_KEY: $CI_SSH_PRIVATE_KEY
    IP_PROD: $CI_IP_PROD
    PORT_PROD: $CI_PORT_PROD
    IP_TEST: $CI_IP_TEST
    PORT_TEST: $CI_PORT_TEST


before_script:
    - echo "Starting before_script"
    - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )' ## Install ssh-agent
    - eval $(ssh-agent -s) ## Run ssh-agent inside the environment
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - ## Add the SSH key stored in SSH_PRIVATE_KEY variable to the agent store
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
    - ssh-keyscan $IP_PROD >> ~/.ssh/known_hosts 
    - ssh-keyscan $IP_TEST >> ~/.ssh/known_hosts 
    - chmod 644 ~/.ssh/known_hosts
    - ssh-keygen -R $IP_PROD
    - ssh-keygen -R $IP_TEST

Back building: #                                    =============== Back build
    image: maven:3.6.3-jdk-8-slim
    stage: build
    cache: # load cache & publish
        key: back
        policy: pull-push
        paths:
            - $MAVEN_PWD
            - MyProj-Backend/target/dependency
    variables:
        MVN_OPTIONS: -DskipTests -Dmaven.repo.local=$MAVEN_PWD # Load maven dependency locally & skip test
    before_script:
        - cd $CI_PROJECT_DIR/MyProj-Backend
    script:
        - mvn install $MVN_OPTIONS
        - mkdir -p target/dependency
        - (cd target/dependency; jar -xf ../*.jar)
    artifacts: # To be able to download the jar.
        when: always
        paths:
            - MyProj-Backend/target/*.jar


Database building: #                                =============== Database image create
    image: docker:latest
    stage: build
    variables:
        DATABASE_NAME: $CI_DB_NAME
        DATABASE_PASSWORD: $CI_DB_PASSWORD
        DOCKER_REPO: $CI_DOCKER_REPO_DB
        DOCKER_OPTIONS: "--build-arg database_name=$DATABASE_NAME --build-arg database_password=$DATABASE_PASSWORD"
    services:
        - docker:dind
    before_script:
        - docker login -u "$DOCKER_LOGIN" -p "$DOCKER_PASS"
        - ls $CI_PROJECT_DIR/Database/
        - cd $CI_PROJECT_DIR/Database/
    script:
        - docker build $DOCKER_OPTIONS -t $DOCKER_LOGIN/$DOCKER_REPO -f Dockerfile.db.dev .
        - docker push $DOCKER_LOGIN/$DOCKER_REPO


Front building: #                                 =============== Front build & testing
    image: node:10.15.0
    stage: build
    cache: # save cache
        key: front
        paths:
            - $CI_PROJECT_DIR/front/dist
    before_script:
        - cd $CI_PROJECT_DIR/front
    script:
        - npm install vue
        - npm install -i
        - npm install
        - npm run build


Back testing: #                                     =============== Back testing
    image: maven:3.6.3-jdk-8-slim
    stage: test
    services:
        - ciemrnt/my-proj_db:latest
    variables:
        DOCKER_LOGIN: $CI_DOCKER_LOGIN
        DOCKER_REPO: $CI_DOCKER_REPO_DB
        DATABASE_NAME: $DB_NAME
        db_host: ciemrnt-my-proj_db #https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#accessing-the-services
        MVN_OPTIONS: -Dmaven.repo.local=/$MAVEN_PWD # Load maven dependency locally if possible
    cache: # load cache
        key: back
        policy: pull
    before_script:
        - cd $CI_PROJECT_DIR/MyProj-Backend
    script:
        - echo $db_host
        - mvn verify $MVN_OPTIONS
    artifacts: # To keep tracks of junit tests
        when: always
        paths:
            - MyProj-Backend/target/*.jar
        reports:
            junit: 
                - MyProj-Backend/target/surefire-reports/TEST-*.xml
                - MyProj-Backend/target/failsafe-reports/TEST-*.xml
        

Back package: #                                     =============== Back package
    image: docker:latest
    stage: package
    only:
        - master
        - pre-production
    variables:
        DOCKER_REPO: $CI_DOCKER_REPO_BACK
    cache: # load cache
        key: back
        policy: pull
        paths:
            - MyProj-Backend/target/dependency
    services:
        - docker:dind
    before_script:
        - cd $CI_PROJECT_DIR/MyProj-Backend
        - docker login -u "$DOCKER_LOGIN" -p "$DOCKER_PASS"
    script:
        - pwd
        - ls
        - docker build -t $DOCKER_LOGIN/$DOCKER_REPO -f Dockerfile.back.prod .
        - docker push $DOCKER_LOGIN/$DOCKER_REPO


Front package: 
    image: docker:latest
    stage: package
    only:
        - master
        - pre-production
    variables:
        DOCKER_REPO: $CI_DOCKER_REPO_FRONT
    cache: # load cache
        key: front
        policy: pull
        paths:
            - $CI_PROJECT_DIR/front/dist
    services:
        - docker:dind
    before_script:
        - cd $CI_PROJECT_DIR/front
        - docker login -u "$DOCKER_LOGIN" -p "$DOCKER_PASS"
    script:
        - docker build -t $DOCKER_LOGIN/$DOCKER_REPO -f Dockerfile.prod .
        - docker push $DOCKER_LOGIN/$DOCKER_REPO



Deploy to TEST:
    stage: deploy
    when: manual
    only:
        - master
        - pre-production
    script:
        - echo "============================== Deployment to test"
        #- scp -o "StrictHostKeyChecking no" -P $PORT_TEST ./docker-compose.prod.yml "root@"$IP_PROD":/opt/my-proj/docker-compose.yml"
        - scp -P $PORT_TEST ./docker-compose.prod.yml "root@"$IP_TEST":/opt/my-proj/docker-compose.yml"
        - ssh root@$IP_TEST -p $PORT_TEST "cd /opt/my-proj; docker-compose down"  #  stop services
        - ssh root@$IP_TEST -p $PORT_TEST "cd /opt/my-proj; docker-compose pull"  #  update services
        - ssh root@$IP_TEST -p $PORT_TEST "cd /opt/my-proj; docker-compose build" #  build services
        - ssh root@$IP_TEST -p $PORT_TEST "cd /opt/my-proj; docker-compose up -d"    #  start services


Deploy to PRODUCTION:
    stage: deploy
    when: manual
    only:
        - master
    script:
        - echo "============================== Deployment to production"
        - scp -P $PORT_PROD ./docker-compose.prod.yml "root@"$IP_PROD":/opt/my-proj/docker-compose.yml"
        - ssh root@$IP_PROD -p $PORT_PROD "cd /opt/my-proj; docker-compose down"  #  stop services
        - ssh root@$IP_PROD -p $PORT_PROD "cd /opt/my-proj; docker-compose pull"  #  update services
        - ssh root@$IP_PROD -p $PORT_PROD "cd /opt/my-proj; docker-compose build" #  build services
        - ssh root@$IP_PROD -p $PORT_PROD "cd /opt/my-proj; docker-compose up -d"    #  start services
```
</details>


## GIT Workflow

|Branch|Description|
|-|-|
|master|Copy of production server, **able to deploy to prod**|
|pre-production|Copy of pre-production (test) server, **able to deploy**|
|dev|Used as main branch for development|
|feature|Special to develop feature|


## Docker

> use **health check** to start the api after the db is initialized

<details>
    <summary>docker-compose dev</summary>

``` DOCKERFILE
version: '2.2'
services: 
    front:
        container_name: front
        image: docker-vue
        build: 
            context: front
            dockerfile: Dockerfile
        ports:
            - 8081:8080
        volumes:
            - ./app:/app
            - /var/www/docker-vue/node_modules
        environment:
            - npm_config_unsafe_perm=true
        ports:
            - "8081:8080"
        depends_on: 
            - server
    server:
        container_name: backend
        restart: on-failure
        image: openjdk:8-jdk-alpine
        volumes: 
            - ./MyProj-Backend/target:/home
        build: 
            context: ./MyProj-Backend
            dockerfile: Dockerfile.back.dev
            args: 
                db_host: database
        expose:
            - 8080
        ports: 
            - 8080:8080
        depends_on: 
            database:
                condition: service_healthy
    database:
        container_name: database
        image: mysql:latest
        volumes:
            - dbVolume:/var/lib/mysql
        build:
            context: ./Database
            dockerfile: Dockerfile.db.dev
            args:  
                database_password: pass
                database_name: MyProj_database
                path: .
        expose:
            - 3306
        ports:
            - 3306:3306
        healthcheck:
            test: "/usr/bin/mysql --user=admin --password=pass --execute \"SHOW DATABASES;\""
            interval: 5s
            timeout: 20s
            retries: 10


volumes:
    dbVolume:
```

</details>

<details>
    <summary>docker-compose prod</summary>

``` DOCKERFILE
version: '2.2'
services: 
    server:
        image: ciemrnt/my-proj_back:latest
        container_name: backend
        restart: on-failure
        expose:
            - 8080
        ports: 
            - 8080:8080
        depends_on: 
            database:
                condition: service_healthy
    database:
        image: ciemrnt/my-proj_db:latest
        container_name: database
        volumes:
            - dbVolume:/var/lib/mysql
        expose:
            - 3306
        ports:
            - 3306:3306
        healthcheck:
            test: "/usr/bin/mysql --user=admin --password=pass --execute \"SHOW DATABASES;\""
            interval: 5s
            timeout: 20s
            retries: 10
    front:
        image: ciemrnt/my-proj_front
        container_name: front
        expose:
            - 80
        ports: 
            - 80:80


volumes:
    dbVolume:
```

</details>

## Database

> **MySql** database, `MyProj_database` schema with a `User` table.

``` Dockerfile
from mysql:latest

USER root
EXPOSE 3306 3306

ARG path="."
ARG database_name
ARG database_password

ENV MYSQL_ROOT_PASSWORD ${database_password}
ENV MYSQL_DATABASE ${database_name}

COPY ${path}/init.sql /docker-entrypoint-initdb.d/init.sql
```

<details>
<summary>init.sql</summary>

``` sql
CREATE SCHEMA IF NOT EXISTS `MyProj_database`  DEFAULT CHARACTER SET `utf8` COLLATE `utf8_unicode_ci`;

CREATE USER 'admin'@'localhost' IDENTIFIED BY 'pass';
CREATE USER 'admin'@'%' IDENTIFIED BY 'pass';

GRANT ALL ON *.* TO 'admin'@'localhost';
GRANT ALL ON *.* TO 'admin'@'%';

CREATE TABLE IF NOT EXISTS `MyProj_database`.`User` (
    `ID` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
    `Name` LONGTEXT NOT NULL,
    `Email` LONGTEXT NOT NULL,
    `Creation` DATE NULL DEFAULT NULL,
    PRIMARY KEY (`ID`),
    UNIQUE INDEX `ID_UNIQUE` (`ID` ASC)
)
AUTO_INCREMENT = 0
DEFAULT CHARACTER SET = utf8;

DELIMITER $$
USE `MyProj_database`$$
DROP TRIGGER IF EXISTS `MyProj_database`.`MyProj_database_BEFORE_INSERT` $$
CREATE DEFINER=`admin`@`%` TRIGGER `MyProj_database`.`User_BEFORE_INSERT` BEFORE INSERT ON `User` FOR EACH ROW
BEGIN
    SET NEW.Creation = NOW();
END;$$
```

</details>

## Back-end

> A **java spring boot rest api** that have one path for **ping**, one to **add a user**, one to **list all users** and some **junit**

<h4> Dockerfile dev </h4>

``` DOCKERFILE
FROM openjdk:8-jdk-alpine

LABEL MAINTAINER="clement.royer"

USER root

WORKDIR /home

ARG JAR_FILE=target/*.jar
ARG db_host

ENV db_host=${db_host}

EXPOSE 8080

COPY ${JAR_FILE} poc-0.0.1-SNAPSHOT.jar

ENTRYPOINT ["java","-jar","/home/poc-0.0.1-SNAPSHOT.jar"]
```

<h4>Dockerfile prod</h4>

``` DOCKERFILE
FROM openjdk:8-jdk

EXPOSE 8080 8080

ARG PWD="."
ARG DEPENDENCY=${PWD}/target/dependency
ARG db_host=database

ENV db_host=${db_host}

# All libs 
COPY ${DEPENDENCY}/BOOT-INF/lib /app/lib

# Config files
COPY ${DEPENDENCY}/META-INF /app/META-INF

# Application classes
COPY ${DEPENDENCY}/BOOT-INF/classes /app

RUN echo ${db_host}

# Start the application by targetting the principal class, and the where libs are.
ENTRYPOINT ["java","-cp","app:app/lib/*","com.devops.poc.PocApplication"]
```

## Front-end

> a **vue js** with one page to **add user** and **list users**

<h4> Dockerfile dev </h4>

``` DOCKERFILE
FROM node:10.15.0

ENV CONTAINER_PATH /var/www/docker-vue

WORKDIR $CONTAINER_PATH

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 8080

CMD ["npm", "run", "serve"]
```

<h4> Dockerfile prod</h4>

``` DOCKERFILE
FROM nginx:latest

COPY dist /usr/share/nginx/html
```