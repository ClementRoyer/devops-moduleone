package com.devops.poc.ressources.input;

import lombok.Data;

@Data
public class NewUser {
    private String name;
    private String email;

    public NewUser(String _name, String _email) {
        name = _name;
        email = _email;
    }
}
