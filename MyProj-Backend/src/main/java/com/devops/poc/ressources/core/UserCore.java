package com.devops.poc.ressources.core;

import com.devops.poc.ressources.database.user.User;
import com.devops.poc.ressources.database.user.UserRepository;
import com.devops.poc.ressources.input.NewUser;
import com.devops.poc.ressources.output.UserProfile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserCore {

    @Autowired
    private UserRepository userRepository;

    public User create(NewUser newUser)
    {
        // no verification bc just a POC
        User user = new User(newUser);
        userRepository.save(user);
        return user;
    }

    public List<UserProfile> userList()
    {
        List<UserProfile> users = new ArrayList<>();
        userRepository.findAll().forEach(user -> users.add(new UserProfile(user)));
        return users;
    }
}
