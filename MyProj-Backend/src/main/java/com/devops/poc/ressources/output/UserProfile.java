package com.devops.poc.ressources.output;

import com.devops.poc.ressources.database.user.User;
import lombok.Data;

import java.util.Date;

@Data
public class UserProfile {
    String name;
    String email;
    Date creation_date;

    public UserProfile(User user)
    {
        name = user.getName();
        email = user.getEmail();
        creation_date = user.getCreation();
    }
}
