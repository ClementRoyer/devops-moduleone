package com.devops.poc.ressources.output;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class BasicOutput {
    private String message;

}
