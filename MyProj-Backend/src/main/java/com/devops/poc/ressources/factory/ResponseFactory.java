package com.devops.poc.ressources.factory;

import com.devops.poc.ressources.output.BasicOutput;
import lombok.NoArgsConstructor;

@NoArgsConstructor
public class ResponseFactory {

    public BasicOutput simple(String message) {
        return new BasicOutput(message);
    }

}
