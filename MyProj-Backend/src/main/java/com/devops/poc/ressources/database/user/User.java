package com.devops.poc.ressources.database.user;

import com.devops.poc.ressources.input.NewUser;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Setter
@Getter
@NoArgsConstructor
@Table(name = "`User`")
public class User {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int ID;

    private String Name;
    private String Email;
    private Date Creation;

    public User(NewUser newUser) {
        Name = newUser.getName();
        Email = newUser.getEmail();
    }
}
