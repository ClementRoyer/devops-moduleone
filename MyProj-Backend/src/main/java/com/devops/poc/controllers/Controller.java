package com.devops.poc.controllers;

import com.devops.poc.ressources.core.UserCore;
import com.devops.poc.ressources.factory.ResponseFactory;
import com.devops.poc.ressources.input.NewUser;
import com.devops.poc.ressources.output.BasicOutput;
import com.devops.poc.ressources.output.UserProfile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.MediaType;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/")
public class Controller {

    @Autowired
    private UserCore userCore;

    private final ResponseFactory responseFactory = new ResponseFactory();

    @GetMapping("ping")
    public BasicOutput ping() {
        return responseFactory.simple("pong - test");
    }

    @PostMapping(path = "/registration", produces = MediaType.APPLICATION_JSON_VALUE)
    public BasicOutput registration(@RequestBody NewUser newUser)
    {
        userCore.create(newUser);
        return responseFactory.simple("User registered");
    }

    @GetMapping("users")
    public List<UserProfile> listUsers()
    {
        return userCore.userList();
    }
}
