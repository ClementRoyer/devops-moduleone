package com.devops.poc.controllers;

import static org.assertj.core.api.Assertions.assertThat;

import com.devops.poc.ressources.core.UserCore;
import com.devops.poc.ressources.database.user.User;
import com.devops.poc.ressources.input.NewUser;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class ControllerTest {

    @Autowired
    private Controller controller;

    @Autowired
    private UserCore userCore;

    @Test
    public void contextLoads() {
        assertThat(controller).isNotNull();
    }

    @Test
    void ping() {
        assertThat(controller.ping().getMessage()).isEqualTo("pong - test");
    }

    @Test
    void addUser() {
        User user = userCore.create(new NewUser("Clément", "clement.royer@epitech.eu"));
        Boolean contains = userCore.userList().stream().anyMatch(userProfile ->
                userProfile.getEmail().equals(user.getEmail()));
        assertThat(contains).isEqualTo(true);
    }
}